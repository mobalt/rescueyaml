#################################################
pulmonary_cmp_header:
    section:  "Lung Related Complications/Outcomes"
#################################################

chest_related_cmp:
    label: "Chest Related Complication(s)"
    checkbox:
        - 0:  Hemothorax
        - 1:  Pneumothorax
        - 2:  Pneumonia
        - 3:  ~None
        - 4:  **Other**
    definition: |-
        Describes the type of chest-related complication.

chest_related_cmp_date:
    label: Date of Chest-related complications
    type: date_mdy

trach:
    label: Tracheostomy?
    radio:
        - 0:  Yes
        - 1:  ~No
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: |-
        Indicates whether the patient has tracheostomy.

trach_date:
    label: Tracheostomy Date
    type: date_mdy
    logic: |-
        trach = 'Yes'
        and
        ecmo_mode = "V-V ECMO"
    definition: "Indicates the date of tracheostomy."

ventilator_ext_date:
    label: Ventilator Extubation Date
    type: date_mdy
    identifier: TRUE
    logic: >-
        ventilator = "Yes"
    definition: |-
        Indicate the date (mm/dd/yyyy) ventilatory support initially ceased after surgery.
        The following guidelines apply:
        Capture the extubation closest to the surgical stop time.
        If the patient has a tracheostomy and is separated from the mechanical ventilator postoperatively within the hospital admission, capture the date and time of separation from the mechanical ventilator closest to the surgical stop time.
        If the patient expires while intubated or cannulated and on the ventilator, capture the date and time of expiration.
        If patient is discharged on chronic ventilatory support, capture the date and time of discharge.

ext_pre_decann:
    label: Extubation Prior to Decannulation?
    radio:
        - 0:  Yes
        - 1:  ~No
    logic: >-
        ventilator = "Yes"
    definition: |-
        Please indicate whether the patient was extubated prior to ECMO decannulation.

re_intubation:
    label: Reintubation?
    radio:
        - 0:  Yes
        - 1:  ~No
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: |-
        Indicates whether the patient was re-intubated.

re_intubation_date:
    label: Reintubation Date
    type: date_mdy
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: "Indicates the date of re-intubation."

pulm_outcomes:
    label: Pulmonary Outcomes
    checkbox:
        - 1:  Oxygen therapy at time of discharge
        - 4:  Discharged to At-home Oxygen
        - 5:  Recovery (breathing room air)
        - 3:  ~None
        - 2:  **Other**
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: |-
        Indicate the outcome of the pulmonary complications (Columbia)

#################################################

blood_cmp_header:
    section:  Vascular Complications

vasc_cmp_type:
    label: Vascular Complication Type
    checkbox:
        - 0:  Bleeding that requires PRBC transfusion or invasive procedures
        - 1:  DVT
        - 2:  Hyperperfusion
        - 3:  Hypoperfusion/limb ischemia
        - 4:  ~None
        - 5:
                name:  vasc_cmp_otr
    definition: |-
        The type of vascular complication that occurred ipsilateral to insertion site while patient is supported by ECMO. (if applicable)

vasc_cmp_date:
    label: "Vascular Complication: Date"
    type: date_mdy
    logic: >-
        {{#notBlank vasc_cmp_type}}
    definition: |-
        Indicate the date when the patient has 1st episode of vascular complication (as defined above) in the duration of ECMO support. (Columbia)

vasc_cmp_treat_bleed:
    label: >-
        Vascular Complication: Treatment for Bleeding
    checkbox:
        - 0:  Relocation of cannula
        - 1:  Vessel repair
    logic: >-
        vasc_cmp_type = "Bleeding that requires PRBC transfusion or invasive procedures"
    definition: Indicate the treatment for bleeding.

vasc_cmp_treat_hyperperf:
    label: |-
        Vascular Complication: Treatment for Hyperperfusion
    checkbox:
        - 0:  Adding additional drainage cannula
        - 1:  Distal banding
        - 2:  Relocation of venous cannula
    logic: >-
        vasc_cmp_type = "Hyperperfusion"
    definition: |-
        Indicate the treatment patient received for hyperperfusion. Hyperperfusion is defined as ipsilateral, to the side of the arterial cannulation, edematous limb that was hyperemic and warm to touch. (http://www.sciencedirect.com/science/article/pii/S0022522312010872)

vasc_cmp_treat_hypoperf:
    label: |-
        Vascular Complication: Treatment for Hypoperfusion/Limb Ischemia
    checkbox:
        - 0:  Additional perfusion cannula
        - 1:  Amputation
        - 2:  Fasciotomy
        - 3:  Relocation of cannula
        - 4:  Vascular repair
    logic: >-
        vasc_cmp_type = "Hypoperfusion/limb ischemia"
    definition: |-
        Lack of arterial signal, lack of cap refill, extremity neurologic dysfunction, clinical suspicion of threatened limb

thrombotic_cmp:
    label: Thrombotic Complications
    checkbox:
        - 0:  Deep Venous Thrombosis
        - 1:  Pulmonary Embolism
        - 2:  Clinically Significant Coagulopathy
        - 3:  Disseminated Intravascular Coagulopathy
        - 4:  ~None
    note: >-
        While one ECMO unless otherwise specified
    definition: |-
        This includes venous and arterial thrombotic complications.

thrombotic_cmp_date:
    label: "Thrombotic Complication: Date"
    type: datetime_mdy
    logic: >-
        {{#notBlank thrombotic_cmp}}
    definition: |-
        Indicate the date when the thrombotic complication occurred.

lv_thrombus:
    label: LV Thrombus?
    radio:
        - 0:  Yes
        - 1:  ~No
    definition: |-
        Indicates whether an LV Thrombus occurred.

lv_thrombus_date:
    label: Date of LV Thrombus
    type: date_mdy
    logic: >-
        lv_thrombus = "Yes"

circuit_related_hemolysis_ldh:
    label: |-
        Circuit Related Hemolysis LDH (if applicable)
    type: number
    definition: |-
        A plasma-free hemoglobin value that is greater than 40 mg/dl, in association with clinical signs associated with hemolysis (e.g., anemia, low hematocrit, hyperbilirubinemia) occurring after the first 72 hours post-implant.  Hemolysis related to documented non-device-related causes (e.g. transfusion or drug) is excluded from this definition. (INTERMACS)

bleedev_src:
    name: "bleedev_{{index}}_src"
    label: "Major Bleeding Event {{index}}: Source"
    radio:
        - 0:  Cannula Site Bleed
        - 1:  ENT - Nose bleed
        - 2:  ENT - Oropharyngeal Bleed
        - 3:  ENT - Unspecified / Other
        - 4:  "GI - Upper (esophagus, stomach, duodenum, small bowel)"
        - 5:  "GI - lower (colon, rectum, anus)"
        - 6:  "GI - unknown, but guaiac positive stools"
        - 7:  Intra-abdominal
        - 8:  Mediastinal - chest wall
        - 9:  Mediastinal - outflow-aorta anastomosis
        - 10: Mediastinal - outflow conduit
        - 11: Mediastinal - inflow conduit
        - 12: Mediastinal - aortic-venous cannulation site
        - 13: Mediastinal - coagulopathy with no surgical site
        - 14: Mediastinal - other surgical site
        - 15: Mediastinal - Unspecified
        - 16: Pleural space
        - 17: Pulmonary
        - 18: Pump pocket
        - 19: Urinary tract
        - 20: ~None
        - 21: **Other**
    logic_1: ""
    logic: >-
        {{#notBlank bleedev_{{#minusOne {{index}}}}_src}}
    definition: |-
        The suspected source of the major bleeding episode, while the patient was on ECMO support. (if applicable).
        An episode of SUSPECTED INTERNAL OR EXTERNAL BLEEDING that results in one or more of the following:
        Death
        Re-operation
        Hospitalization
        Transfusion of Packed Red Blood Cells (PBRBC) as follows:
        Within 7 days after cannulation:
        If  weight ≥ 50 kg and received ≥ 4 U of PRBC within any 24 h period
        If weight < 50 kg and received ≥ 20 cc/kg of PRBC within any 24 h period
        Or, if after 7 days post cannulation
        The investigator should record the number of units given.
        Note: Hemorrhagic stroke is considered a neurological event and not as a separate bleeding event.

bleedev_date:
    name: "bleedev_{{index}}_date"
    label: "Major Bleeding Event {{index}}: Date"
    type: date_mdy
    identifier: TRUE
    logic: >-
        {{#notBlank bleedev_{{index}}_src}}
    definition: |-
        The date of the 1st major bleeding episode (as defined above), while the patient was on ECMO support.

bleedev_inr:
    name: "bleedev_{{index}}_inr"
    label: "Major Bleeding Event {{index}}: INR"
    type: number
    logic: >-
        {{#notBlank bleedev_{{index}}_src}}
    definition: |-
        The INR value at time of major bleeding episode.

bleedev_reop:
    name: "bleedev_{{index}}_reop"
    label: |-
        Major Bleeding Event {{index}}: resulted in Reoperation?
    radio:
        - 0:  Change of Cannula Site
        - 1:  Re-exploration
        - 2:  Repair
        - 3:  ~No
    logic: >-
        {{#notBlank bleedev_{{index}}_src}}
    definition: |-
        Indicates the type of re-operation that was performed as a consequence of the 1st major bleeding event. (if applicable)

bleedev_transfusion:
    name: "bleedev_{{index}}_transfusion"
    label: "Major Bleeding Event {{index}}: Transfusion?"
    radio:
        - 0:  Yes
        - 1:  ~No
    logic: >-
        {{#notBlank bleedev_{{index}}_src}}
    definition: |-
        Indicate whether the major bleeding event results in transfusion of red blood cells that satisfies the following rules:
        Transfusion of Packed Red Blood Cells (PBRBC) as follows:
        Within 7 days after cannulation:
        If  weight ≥ 50 kg and received ≥ 4 U of PRBC within any 24 h period
        If weight < 50 kg and received ≥ 20 cc/kg of PRBC within any 24 h period
        Or, if after 7 days post cannulation
        The investigator should record the number of units given.

bleedev_transfusion_amount:
    name: "bleedev_{{index}}_transfusion_amount"
    label: |-
        Major Bleeding Event {{index}}: Transfusion Amount
    type: number
    note: Units of PRBC
    logic: >-
        bleedev_{{index}}_transfusion ='Yes'
    definition: |-
        The number of units of Packed Red Blood Cells (PRBC) transfused into the patient.

bleedev_anticoag:
    name: "bleedev_{{index}}_anticoag"
    label: "Major Bleeding Event {{index}}: Anticoagulation"
    radio:
        - 0:  Argatroban
        - 1:  Aspirin
        - 2:  Bivalirudin
        - 3:  Clopidogrel (Plavix)
        - 4:  Dextran
        - 5:  Dipyridamole
        - 6:  Fondaparinux
        - 7:  Heparin
        - 8:  Hirudin
        - 9:  Lepirudin
        - 10: Lovenox
        - 11: Ticlopidine
        - 12: Warfarin
        - 13: Ximelagatran
        - 14: ~None
        - 15: **Other**
    logic: >-
        {{#notBlank bleedev_{{index}}_src}}
    definition: |-
        Indicate the type of anticoagulation the patient was on at the time of the event. (if applicable)

bleedev_resolution:
    name: "bleedev_{{index}}_resolution"
    label: "Major Bleeding Event {{index}}: Resolution"
    radio:
        - 0:  "Complete Cessation of Anticoagulation (Heparin, Bivalirudin, etc.)"
        - 1:  "Blood transfusion (ffp/cryo/platelets)"
        - 2:  "Procoagulant(Factor VII, PCC etc)"
        - 3:  Packing/topical pressure
        - 4:  "Surgical intervention of hemostasis (including cauterization, suture hemostasis)"
        - 6:  ~None
        - 5:  **Other**
    logic: >-
        {{#notBlank bleedev_{{index}}_src}}
    definition: |-
        Indicates how the 1st Major Bleeding episode was resolved.


#################################################
neuro_header:
    section:  Neurologic Complication
#################################################

neuro_cmp:
    label: Neurologic Complication Type
    checkbox:
        - 0:  Diffuse Cerebral Edema/Hypoxic Encephalopathy
        - 1:  Encephalopathy and/or delirium at discharge
        - 2:  Intracranial Hemorrhage/Hemorrhagic Stroke
        - 3:  Ischemic Stroke/Embolization
        - 4:  Seizures
        - 5:  ~None
        - 6:  **Other**
    definition: |-
        Any new, temporary or permanent, focal or global neurological deficit ascertained by a standard neurological examination (administered by a neurologist or other qualified physician and documented with appropriate diagnostic tests and consultation note). The examining physician will distinguish between a transient ischemic attack (TIA), which is fully reversible within 24 hours (and without evidence of infarction), and a stroke, which lasts longer than 24 hours (or less than 24 hours if there is evidence of infarction). Each neurological event must be subcategorized as:
        1) Transient Ischemic Attack (acute event that resolves completely within 24 hours with no evidence of infarction)
        2) Ischemic or Hemorrhagic Cerebral Accident/CVA (event that persists beyond 24 hours or less than 24 hours associated with infarction on an imaging study. (Columbia)

neuro_cmp_date:
    label: "Neurological Complication: Date"
    type: date_mdy
    logic: >-
        {{#notBlank neuro_cmp}}
    definition: |-
        Indicate the date of onset of the adverse neurologic event. (Columbia)

#################################################
renal_cmp_header:
    section:  Renal Complications
#################################################

reminder_chronic_dialysis:
    label: |-
        Reminder: Chronic dialysis was added in form #2.
    type: descriptive
    logic: >-
        past_medical_hx = "End-stage Renal Disease requiring dialysis"
    definition: |-
        Indicate whether the patient is on chronic dialysis prior to ECMO implantation.(Columbia)

renal_cmp:
    label: Acute Renal Dysfunction?
    radio:
        - 0:  Yes
        - 1:  ~No
    definition: |-
        Two categories of renal dysfunction will be identified:
        Acute Renal Dysfunction
        Abnormal kidney function requiring dialysis (including hemofiltration) in patients who did not require this procedure prior to implant, or a rise in serum creatinine of greater than 3 times baseline or greater than 5 mg/dL (in children, creatinine greater than 3 times upper limit of normal for age) sustained for over 48 hours.
        Chronic Renal Dysfunction
        An increase in serum creatinine of 2 mg/dl or greater above baseline, or requirement for hemodialysis sustained for at least 90 days
        (INTERMACS)

renal_cmp_date:
    label: Renal Complication Date
    type: date_mdy
    logic: >-
        renal_cmp = "Yes"
    definition: |-
        Indicate the date when the patient meets the criteria of renal complications for the first time(?) postoperatively during ECMO support.  (Columbia)

cre_discharge:
    label: Creatinine at Discharge
    type: number
    definition: |-
        A measure of the creatinine at time of discharge.

dialysis_discharge:
    label: Dialysis at Discharge?
    radio:
        - 0:  Yes
        - 1:  ~No
    definition: |-
        Indicates whether the patient was on chronic dialysis at time of discharge.

#################################################
infectious_cmp_header:
    section:  Infection
#################################################

infect_site:
    name: "infect_{{index}}_site"
    label: "Infectious Complication {{index}}: Site"
    checkbox:
        - 1:  >-
            <b>Blood Stream</b>
            - Bacteremia
        - 2:  >-
            <b>Blood Stream</b>
            - Sepsis
        - 3:  >-
            <b>Eyes/Ears</b>
            - Conjunctivitus, Retinitus, Otitis
        - 4:  >-
            <b>Gastrointestinal Tract</b>
        - 5:  >-
            <b>Heart</b>
            - Endocarditis
        - 7:  >-
            <b>Respiratory</b>
            - Rhinitis, Sinusitis, Pharyngitis, Bronchitis, Pneumonia
        - 9:  >-
            <b>Skin</b> - IV Line Site
        - 8:  >-
            <b>Skin</b> - Peripheral Cannulation Site
        - 13: >-
            Surgical Wound Infection - Incisional
        - 14: >-
            Surgical Wound Infection - Deep (Mediastinitis)
        - 10: >-
            <b>Urinary Tract</b>
            - Vaginitis, Cervicitis, Urethritis, Cystitis, Pyelonephritis
        - 11: ~None
        - 12: **Other**
    logic: >-
        {{#notBlank infect_{{#minusOne {{index}}}}_site}}
    logic_1: ""

infect_micro:
    name: "infect_{{index}}_micro"
    label: "Infectious Complication {{index}}: Microbiology"
    checkbox:
        - 1:  "Bacilli, Gram-negative"
        - 2:  Candida spp.
        - 21: Clostridium dificile
        - 3:  Cornyebacterium spp.
        - 4:  "Cytomegalovirus (CMV)"
        - 5:  Escherichia coli
        - 6:  "Enterobacteriaceae (Klebsiella, Proteus, Salmonella, Serratia, Shigella, Yersinia)"
        - 7:  Enterococci (non-VRE)
        - 8:  Histoplasma
        - 9:  Influenza
        - 10: Listeria
        - 11: Micrococci
        - 12: Propionibacterium acnes
        - 13: Pseudomonas aeruginosa
        - 14: "Straphylococcus aureus - <b>MRSA</b>"
        - 19: "Straphylococcus aureus - <b>MSSA</b>"
        - 15: Staphylococcus spp.
        - 16: Streptococcus spp.
        - 20: "Vancomycin-Resistant Enterococci (<b>VRE</b>)"
        - 17: ~None/Indeterminant
        - 18: **Other**
    logic: >-
        {{#notBlank infect_{{index}}_site}}

infect_date:
    name: "infect_{{index}}_date"
    label: "Infectious Complication {{index}}: Date"
    type: date_mdy
    logic: >-
        {{#notBlank infect_{{index}}_site}}

#################################################
circuit_cmp_header:
    section:  Circuit Related Technical Complications
#################################################

circuit_related_technical_cmp:
    label: Circuit Related Technical Complications or Device Malfunction
    checkbox:
        - 0:  Air Embolism
        - 1:  Cannula Dislodgement
        - 2:  Oxygenator Failure requiring Exchange
        - 3:  Pump Malfunction
        - 4:  Thrombosis requiring circuit change
        - 5:  Tubing Rupture
        - 6:  ~None
    definition: |-
        Indicates the type of technical complication related to the ECMO circuit.

device_malfunction_date:
    label: "Device Complication: Date"
    type: date_mdy
    identifier: TRUE
    logic: >-
        {{#notBlank circuit_related_technical_cmp}}
    definition: |-
        Indicate the date of Circuit-related Technical complications or Device Malfunction.

device_malfunction_cause:
    label: "Device Complication: Cause"
    type: text
    logic: >-
        {{#notBlank circuit_related_technical_cmp}}
    definition: |-
        Indicate the cause of device malfunction.(Columbia)


#################################################
other_complications_header:
    section:  Other Complications
#################################################

otr_cmp:
    label: Other Complications
    checkbox:
        - 0:  Peripheral Wound
        - 1:  PNA
    definition: |-
        Indicate all other complications if they have not been included in all above sections. (Columbia)


#################################################
oxygenator_header:
    section:  Oxygenator Exchange Events
#################################################

oxor_ex_reason:
    name: "oxor_ex_{{index}}_reason"
    label: "Oxygenator Exchange {{index}}: Reason"
    checkbox:
        - 0:  Blood Pressure Change
        - 1:  Clot Formation
        - 2:  Device Failure
        - 3:  Membrane Leak
        - 4:  Poor Oxygenation
        - 5:  Suspected Oxygenator Dysfunction
        - 6:  ~None/Unspecified
        - 7: **Other**
    logic_1: >-
        circuit_related_technical_cmp = "Oxygenator Failure requiring Exchange"
    logic: >-
        {{#notBlank oxor_ex_{{#minusOne {{index}}}}_reason}}
    definition: |-
        Indicate the reasons of 1st time exchange of oxygenator. (Columbia)

oxor_ex_date:
    name: "oxor_ex_{{index}}_date"
    label: "Oxygenator Exchange {{index}}: Date"
    type: date_mdy
    logic: >-
        {{#notBlank oxor_ex_{{index}}_reason}}
    definition: |-
        Indicate the date of exchange (if there are multiple exchanges, the 1st exchange event) of oxygenator. (Columbia)

oxor_ex_type:
    name: "oxor_ex_{{index}}_type"
    label: "Oxygenator Exchange {{index}}: Replacement's Type"
    radio:
        - 0:  Quadrox-1
        - 1:  **Other**
    logic: >-
        {{#notBlank oxor_ex_{{index}}_reason}}
    definition: |-
        Indicate the type of new oxygenator used in the 1st exchange. (Columbia)
