#################################################
gen_info_header:
    section: General ECMO Information
#################################################


ecmo_init_date_time:
    label: "ECMO Initiation: Date & Time"
    note: 24hr time
    type: datetime_mdy
    identifier: TRUE
    definition: |-
        The date and time of ECMO initiation, in the conventional US format (MM/DD/YYYY HH:mm). When the exact time is unknown, use noon (12:00) or best approximation from patient's chart.

ecmo_discontinuation_date_time:
    label: "ECMO Discontinuation: Date & Time"
    note: 24hr time
    type: datetime_mdy
    identifier: TRUE
    definition: |-
        The date and time of ECMO was discontinued, in the conventional US format (MM/DD/YYYY HH:mm). When the exact time is unknown, use noon (12:00) or best approximation from patient's chart.

ecmo_los:
    label: ECMO Length of Support
    formula: >-
        datediff([ecmo_init_date_time], [ecmo_discontinuation_date_time], "d", "mdy")
    definition: |-
        The number of days the patient remained on ECMO, auto-calculated by finding the difference between ECMO discontinuation and Initiation.

cann_physician_specialty:
    label: Cannulating Physician Specialty
    radio:
        - 0:  Cardiac surgeon
        - 1:  Cardiologist
        - 2:  Intensivist
        - 3:  Trauma surgeon
    logic: >-
        study_design = "Prospective"
    definition: |-
        The specialty of the cannulating physician.

cann_loc:
    label: Cannulation Location
    radio:
        - 0:  Cath lab
        - 1:  Emergency Department
        - 2:  ICU
        - 3:  Non-ICU WARD
        - 4:  Operating Room
        - 5:  OSH Transfer, already on ECMO
        - 6:  Team goes to OSH → cannulates patient → transfers to Primary Facility
        - 7:  **Other**
    definition: |-
        The location in which the ECMO cannulation took place.


#################################################
ecmo_mode_header:
    section: ECMO Mode
#################################################

ecmo_initial_mode:
    label: Initial ECMO Mode
    radio:
        - 0:  V-V ECMO
        - 1:  V-A ECMO
        - 2:  V-A-V ECMO
        - 3:  LVAD ECMO (LV or LA/Aorta)
        - 4:  RVAD ECMO (RA/PA)
        - 5:  PA to LA ECMO - pumpless
        - 6:  PA to LA ECMO - with pump
        - 7:  **Other**
    definition: |-
        D0, P and R, see below
        V-V ECMO =  venovenous ECMO for respiratory support with single site or multi site cannulation
        V-A ECMO = veno-arterial ECMO for cardiopulmonary support
        V-A-V ECMO = hybrid ECMO support with two return cannulas (on arterial and one venous). Circuit blood return to both venous and arterial systems to provide a combination of hemodynamic support and partial VV ECMO support
        LVAD ECMO (LV or LA/Aorta) = Oxygenator is spliced into a temporary LVAD circuit to provide respiratory support in addition to the LVAD support
        RVAD ECMO (RA/PA) = Oxygenator is spliced into a temporary RVAD circuit to provide respiratory support in addition to the RVAD support
        PA (pulmonary artery) to LA (left atrium) ECMO — pumpless  = To support patients with RV failure from IPAH
        PA (pulmonary artery) to LA (left atrium) ECMO — with pump = To support patients with RV failure from IPAH
        Other



# Conversion Sub-templates
ecmo_conv_type:
    name: "ecmo_conv_{{index}}"
    label: "ECMO Conversion {{index}}"
    radio:
        - 0:  V-V ECMO
        - 1:  V-A ECMO
        - 2:  V-A-V ECMO
        - 3:  LVAD ECMO (LV or LA/Aorta)
        - 4:  RVAD ECMO (RA/PA)
        - 5:  ~None
        - 6:  **Other**
    logic_1: ""
    logic: >-
        {{#notBlank ecmo_conv_{{#minusOne {{index}}}}}}

ecmo_conv_date:
    name: "ecmo_conv_{{index}}_date"
    label: "ECMO Conversion {{index}}: Date"
    type: datetime_mdy
    identifier: TRUE
    logic: >-
        {{#notBlank ecmo_conv_{{index}}}}
    definition: |-
        If applicable, the ECMO mode into which the patient was converted after the initial ECMO mode.
        E.g., if the patient is on VA ECMO and is transitioned to durable LVAD but requires RVAD ECMO, this would be considered an ECMO conversion and one would select RVAD ECMO in this field.


#################################################
cannula_technical_info_header:
    section:  Cannula Technical Information
#################################################



art_acc:
    name: "art_acc_{{index}}"
    label: "Arterial Limb Access {{index}}"
    radio:
        - 0:  Aorta (Central)
        - 1:  Axillary Artery
        - 2:  Femoral Artery
        - 3:  Open or Percutaneous
        - 4:  ~None
        - 5:
            definition: |-
                Alternate sites for arterial access such as the iliac artery, left atrial return in the case of PA to LA ECMO.
    logic_1: ""
    logic: >-
        {{#notBlank art_acc_{{#minusOne {{index}}}}}}
    definition: |-
        The location used to gain arterial access for VA ECMO. In the case of VV-ECMO, select "none"

art_acc_cann_size:
    name: "art_acc_{{index}}_cann_size"
    label: "Arterial Limb Access {{index}}: Cannula Size"
    radio:
        - 0:  15 Fr
        - 1:  16 Fr
        - 2:  17 Fr
        - 3:  18 Fr
        - 4:  19 Fr
        - 5:  20 Fr
        - 6:  21 Fr
        - 7:  22 Fr
        - 8:  23 Fr
        - 9:  24 Fr
    note: French Size
    logic: >-
        {{#notBlank art_acc_{{index}}}}
    definition: >-
        Size in French of direct arterial cannula (does not apply to graft type cannulation).

art_acc_graft_anast:
    name: "art_acc_{{index}}_graft_anast"
    label: "Arterial Limb Access {{index}}: Graft Anastomosis"
    radio:
        - 0:  Open, Direct Cannulation
        - 1:  Graft anastomosis end to side
        - 2:  Cutdown
    logic: >-
        {{#notBlank art_acc_{{index}}}}
    definition: |-
        Specify here if the artery is directly cannulated or a sidearm graft is sewn to the artery.

art_acc_graft_anast_size:
    name: "art_acc_{{index}}_graft_anast_size"
    label: "Arterial Limb Access {{index}}: Graft Anastomosis Size"
    type: number
    note: Diameter in mm
    logic: >-
        {{#notBlank art_acc_{{index}}}}
    definition: |-
        If a sidearm graft is sewn to the artery, please specify its diameter in mm.




# Venous Access Templates
ven_acc:
    name: "ven_acc_{{index}}"
    label: "Venous Limb Access {{index}}"
    radio:
        - 0:  Internal Jugular Vein
        - 1:  Femoral Vein
        - 2:  Right Atrium(Central)
        - 3:  Single site dual lumen VV ECMO
        - 4:  **Other**
    logic_1: ""
    logic: >-
        {{#notBlank ven_acc_{{#minusOne {{index}}}}}}
    definition: |-
        This is the location of the venous drainage cannula for VA ECMO or VV ECMO.

ven_acc_cann_size:
    name: "ven_acc_{{index}}_cann_size"
    label: "Venous Limb Access {{index}}: Cannula Size"
    radio:
        - 0:  15 Fr
        - 1:  16 Fr
        - 2:  17 Fr
        - 3:  18 Fr
        - 4:  19 Fr
        - 5:  20 Fr
        - 6:  21 Fr
        - 7:  22 Fr
        - 8:  23 Fr
        - 9:  24 Fr
        - 10: 25 Fr
        - 11: 26 Fr
        - 12: 27 Fr
        - 13: 28 Fr
        - 14: 29 Fr
        - 15: 30 Fr
        - 16: 31 Fr
    note: French Size
    logic: >-
        {{#notBlank ven_acc_{{index}}}}
    definition: "size of cannula in French."


#################################################
lv_vent_header:
    section: Left Ventricular Vent
#################################################


lv_vent:
    label: Left Ventricular Vent?
    radio:
        - 0:  Yes
        - 1:  ~No
    note: Placed at time of initial cannulation
    definition: |-
        D0, P and R, Should include only LV vent placed at time of cannulation, otherwise specified elsewhere

lv_vent_route:
    label: "Left Ventricular Vent: Route"
    radio:
        - 0:  Aortic Valve
        - 1:  Left Atrium
        - 2:  LV Apex
        - 3:  PA Vent
    logic: >-
        lv_vent = "Yes"
    definition: Route of LV venting

lv_vent_method:
    label: "Left Ventricular Vent: Method"
    radio:
        - 0:  Direct Cannulation
        - 1:  IABP
        - 2:  Impella 2.5
        - 3:  Impella 3.5
        - 4:  Impella 5.0
        - 5:  Septostomy
        - 6:  Trans-septal cannula
        - 7:  **Other**
    logic: >-
        lv_vent = "Yes"
    definition: Method of venting left side

lv_vent_reason:
    label: "Left Ventricular Vent: Reason"
    checkbox:
        - 0:  LV stagnant flow or clot formation
        - 1:  Pulmonary Edema
        - 2:  Refractory ventricular arrhythmia
        - 3:  Routine
        - 4:  **Other**
    logic: >-
        lv_vent = "Yes"
    definition: "The reason for LV venting."

lv_vent_cann:
    label: "Left Ventricular Vent: Cannula Size"
    type: number
    note: French Size
    logic: >-
        lv_vent = "Yes"
    definition: |-
        The size of cannula used if direct cannulation.

lv_vent_date:
    label: "Left Ventricular Vent: Date & Time"
    type: datetime_mdy
    identifier: TRUE
    logic: >-
        lv_vent = "Yes"
    definition: "The date that the LV vent was put in."


#################################################


dist_perf_cann:
    label: Distal Perfusion Cannula?
    radio:
        - 0:  Yes
        - 1:  ~No
    definition: |-
        Indicates whether or not a distal perfusion cannula was used. This cannula is placed to allow perfusion of the extremity ipsilateral to direct arterial cannulation.

        If sidearm grafting or central cannulation is performed, please answer "Yes".

dist_perf_cann_time_initial_cann:
    label: "Distal Perfusion Cannula: Done at the Time of Initial Cannulation?"
    radio:
        - 0:  Yes
        - 1:  ~No
    logic: >-
        dist_perf_cann = "Yes"
    definition: |-
        Indicates whether the distal perfusion cannula was placed at the same time as the initial cannulation. If sidearm grafting is used, please specify "Yes".

dist_perf_cann_date:
    label: "Distal Perfusion Cannula: Date"
    type: date_mdy
    logic: >-
        dist_perf_cann_time_initial_cann = "No"
    definition: |-
        If distal perfusion cannula not placed at time of initial cannulation, please place date of implant here.

dist_perf_cann_indication:
    label: "Distal Perfusion Cannula: Indication"
    radio:
        - 0:  Routine
        - 1:  Selective
    logic: >-
        dist_perf_cann = "Yes"
    definition: |-
        Indicates the reason for adding a distal perfusion cannula.

dist_perf_cann_loc:
    label: "Distal Perfusion Cannula: Location"
    radio:
        - 0:  Axillary artery
        - 1:  Common femoral artery
        - 2:  Posterior Tibial Artery
        - 3:  Superficial Femoral Artery
        - 4:  **Other**
    logic: >-
        dist_perf_cann = "Yes"
    definition: |-
        The insertion site of the limb-saver distal perfusion cannula.

dist_perf_cann_size:
    label: "Distal Perfusion Cannula: Cannula Size"
    radio:
        - 0:  4 Fr
        - 1:  5 Fr
        - 2:  6 Fr
        - 3:  7 Fr
        - 4:  8 Fr
        - 5:  9 Fr
        - 6:  Graft
        - 7:  **Other**
    note: French Size
    logic: >-
        dist_perf_cann = "Yes"
    definition: |-
        The size of the distal perfusion cannula, in French size. If sidearm grafting is used, please specify "Graft".

#################################################


circuit_coating:
    label: Circuit Coating
    radio:
        - 0:  Bioline
        - 1:  Carmeda
        - 2:  Combo
        - 3:  SmartRx
        - 4:  Softline
        - 5:  ~None
        - 6:  **Other**
    definition: |-
        The type of circuit coating that was used. (if applicable)

oxygenator:
    label: Oxygenator
    radio:
        - 0:  Cardiohelp
        - 1:  NovaLung
        - 2:  Quadrox-D
        - 3:  Temporary CPB oxygenator
        - 4:  **Other**
    definition: |-
        The type of oxygenator used in the circuit.

pump:
    label: Pump
    radio:
        - 0:  Cardiohelp
        - 1:  Centrimag
        - 2:  Revolution
        - 3:  Centrifugal pump
        - 4:  Rotaflow
        - 5:  Roller
        - 6:  **Other**
    definition: |-
        The type of pump used in the ECMO circuit.
